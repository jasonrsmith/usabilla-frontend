var usabilla = angular.module('usabilla', ['chart.js', 'ngMap', 'ui.bootstrap', "template/modal/backdrop.html"]);

usabilla.controller('GetController', function GetController($scope, usabillaService, $interval) {
	if($scope.apicall != undefined && $scope.apicall != '') {
		$scope.RefreshServer = function(){
			usabillaService.usabillaRefresh().then(function(response) {
				APICall($scope, usabillaService);
			});
		}
		var APICall = function ($scope, usabillaService){
			console.log($scope.refresh);
			usabillaService.usabillaHttp($scope).then(function(response) {
				if(typeof response.data =='object') {
					angular.extend($scope, response.data);
					if($scope.items != undefined){
						var total = 0;
						var countlength = $scope.items.length;
						for (var i = 0; i < countlength; i++) {
							total += $scope.items[i].rating;
						}
						$scope.average = total/countlength;
						$scope.json_string = JSON.stringify({'items': $scope.items}, null, 2);
					}
					if($scope.ratings_count != undefined){
						$scope.pielabels = [];
						$scope.piedata = [];
						for(var key in $scope.ratings_count) {
						$scope.piedata.push($scope.ratings_count[key]);
						$scope.pielabels.push('Rating ' + key);
						}
					}
				}
			
			});
		}
		if($scope.refresh != undefined && $scope.refresh != '') {
			APICall($scope, usabillaService);
			$interval(function(){
				APICall($scope, usabillaService)
			}, $scope.refresh, [$scope, usabillaService]);
		} else {
			APICall($scope, usabillaService);
		}
    	}

});


usabilla.factory('usabillaService', function($http) {
	var usabillaService = {
		usabillaHttp: function($arguments) {
			var argument = $arguments.apicall;
			var promise = $http.get('/api_endpoint/' + argument).success(function(response){
                		return response;
            		});
			return promise;
        	},
		usabillaRefresh: function() {
			var promise = $http.get('/api_endpoint/refresh').success(function(response){
                		return response;
            		});
			return promise;
        	},
    	};
	return usabillaService;
});

usabilla.directive('usabilla', function usabilla($http, $compile){
	return {
		controller: 'GetController',
		transclude: true,
		scope: {
			'apicall' : '@',
			'template' : '@',
			'refresh' : '@',
		},
    		template: '<div ng-include = "getTemplate()" ng-hide="hide"></div>',
		link : function(scope, element, attrs)
		{
          		scope.getTemplate = function(){
	      			if(scope.template != undefined){
					return '/local_components/templates/' + scope.template;
	      			}
          		}
      		}
  	}
});

